package OS2;

import java.util.*;

public class OS {
	static OS OS=new OS();
	Scanner scan = new Scanner(System.in);
	
	//待ち行列変数群
	String inputstring=null;//入力文字列
	int starttime=0;//開始時間
	int nowtime=0;//現在時間
	int turnaroundtime=0;//ターンアラウンドタイム
	int count=0;//カウント
	int processingtime=0;//処理時間
	int totalturnaroundtime=0;//合計ターンアラウンドタイム
	double averageturnaroundtime=0;//平均ターンアラウンドタイム
	
	public static void main(String[] args) {
		//FIFOのアルゴリズム
		OS.FIFO();
		//SPTFのアルゴリズム
		//OS.STPF();
	}
	
	public void FIFO(){
		System.out.println("数字を入力してください");
		String str=scan.next();
		
		inputstring=str;//入力文字列
		
		//FIFOのアルゴリズム(処理プログラム)
		System.out.println("FIFOのプロセス");
		while(true){
			starttime=starttime+Integer.parseInt(inputstring.substring(0,1));//開始時間			
			nowtime=Integer.parseInt(inputstring.substring(1,2));
			count++;//プロセススタート
			
			//待ち行列の待機状態
			if(processingtime<starttime){
				processingtime=starttime;
			}
			
			processingtime=processingtime+nowtime;//かかった時間
			turnaroundtime=processingtime-starttime;//ターンアラウンドタイム
			totalturnaroundtime=totalturnaroundtime+turnaroundtime;//合計のターンアラウンドタイム
			
			//出力
			System.out.println("プロセス"+count+"回目:"+"開始:"+starttime+"ms,終了:"+processingtime+"ms,ターンアラウンドタイム:"+turnaroundtime+"ms");
			
			inputstring=inputstring.substring(2, inputstring.length());
			
			//終了
 			if(inputstring.length()<2){
 				break;
 			}
		}
		averageturnaroundtime=totalturnaroundtime/count;//平均ターンアラウンドタイム
		System.out.println("平均ターンアラウンドタイム:"+averageturnaroundtime+"ms");
	}
	//STPF
	public void STPF(){
		System.out.println("数字を入力してください");
		String str=scan.next();
		
		inputstring=str;//入力文字列
		String process[]=new String[str.length()/2];//文字列の分割
		int processing[]=new int[str.length()/2];//処理時間
		int a=0,b=1,c=2,d=0,e=1,f=2;
		String temp;
		
		//STPFのアルゴリズム(処理プログラム)
		System.out.println("STPFのプロセス");
		for(int i=0;i<str.length()/2;i++){//2文字ずつ取り出す配列
			process[i] = str.substring(a,a+2);//分割した2文字ずつを配列に代入
			processing[i] = Integer.parseInt(str.substring(b,c)); //文字列の処理時間を代入
			a=a+2; b=b+2; c=c+2;//繰り上げ
		}
		
		for(int i=0;i<processing.length-1;i++){//ソート
			for(int j =i+1;j<processing.length;j++){//並び替え
				if(processing[j]<processing[i]){
					temp=process[i];//tempに一度入れて
					process[i]=process[j];//文字の入れ替え
					process[j]=temp;//tempの文字を後ろに
				}
			}
		}
		
		StringBuffer sb=new StringBuffer();
		for (int i=0;i<process.length;i++) {
			sb.append(process[i]);//配列を文字列に変換
		}
		
		str=sb.toString();//文字列をstrに代入
		
		while(true){
			starttime=starttime+Integer.parseInt(inputstring.substring(d,e));//開始時間			
			nowtime=Integer.parseInt(inputstring.substring(e,f));
			
			//待ち行列の待機状態
			if(processingtime<starttime){
				processingtime=starttime;
			}
			processingtime=nowtime+processingtime;//処理時間
			turnaroundtime=processingtime-starttime;//ターンアラウンドタイム
			count++;
			d=d+2; e=e+2; f=f+2;//読み取る文字をくり上げ
			
			//出力
			System.out.println("プロセス"+count+"回目:"+"開始"+starttime+"ms,終了"+processingtime+"ms,ターンアラウンドタイム"+turnaroundtime+"ms");
			totalturnaroundtime=totalturnaroundtime+turnaroundtime;//合計ターンアラウンドタイム
			
			if(str.length()<f){
				break;
			}
		}
		averageturnaroundtime=totalturnaroundtime/count;//平均ターンアラウンドタイム
		System.out.println("合計ターンアラウンドタイム:"+totalturnaroundtime+"ms");
		System.out.println("平均ターンアラウンドタイム:"+averageturnaroundtime+"ms");
	}
}