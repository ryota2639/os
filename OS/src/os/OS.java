package os;

import java.util.ArrayList;
import java.util.Scanner;
public class OS {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("数字");
		String str = scan.next();

		FIFO(str);
		//STPF(str);
	}
	public static void FIFO(String in){
		String input 	= 	in;	//入力文字列
		int start 		=	0;	//プロセスの開始時間
		int time 		=	0;	//現在時間
		int turnaround 	=	0;	//ターンアラウンドタイム
		int count 		=	0;	//カウント

		while(true){
			//文字列のプロセス生成
			start += Integer.parseInt(input.substring(0, 1));//開始時刻の設定
			int syoriTime = Integer.parseInt(input.substring(1,2));//処理時間
			count ++;
			//現在時間が開始時刻でなかったら待つ
			if(time < start){
				time = start;
			}
 			time += syoriTime;//処理にかかった時間を求める

 			int aroundtime 	=	time - start;//ターンアラウンドタイムを求める
 			turnaround 		+= 	aroundtime;//合計のターンアラウンドタイム

 			System.out.println("プロセス"+count+"開始"+start+"ミリ秒 終了"+time+"ミリ秒 ターンアラウンドタイム"+aroundtime+"ミリ秒");
 			input 			= 	input.substring(2, input.length());

 			//終了
 			if(input.length()<2){
 				break;
 			}

		}
		//ターンアラウンドタイムの平均
		System.out.println("平均ターンアラウンドタイム"+(turnaround/count)+"ミリ秒");
	}
	public static void STPF(String in){
		String input =in;
		ArrayList list =new ArrayList();
		int startTime =0;
		int time =0;
		int turnaround =0;
		int count =0;

		while(true){
			int[] process = new int[2];
			startTime += Integer.parseInt(input.substring(0,1));

			process[0] =startTime;
			process[1] =Integer.parseInt(input.substring(1,2));
			list.add(process);

			input = input.substring(2,input.length());

			if(input.length()<2){
				break;
			}

		}
		while(true){
			int[] pro;
			int proIndex;

			pro = (int[])list.get(0);
			proIndex =0;

			if(pro[0]>time){
				time =pro[0];
			}
			for(int i=0;i<list.size();i++){
				int[] p = (int[])list.get(i);

				if(p[0] <= time){
					if(pro[1]>p[1]){
						pro =p;
						proIndex =i;
					}
				}else{
					break;
				}
			}
		time += pro[1];
		count ++;
		int ta = time -pro[0];
		turnaround +=ta;
		System.out.println("プロセス"+count+"開始"+pro[0]+"ミリ秒 終了"+time+"ミリ秒 ターンアラウンドタイム"+ta+"ミリ秒");
		list.remove(proIndex);
			if(list.size() <= 0){
				break;

			}
		}
		System.out.println("平均ターンアラウンドタイム"+(turnaround/count)+"ミリ秒");
	}

}